
import React from "react";
import { Nav,Container, Row, Col, Card, Form, Button } from "react-bootstrap";
import { withRouter } from "react-router";


const Side = props => {
    return (
        <div className ='menu-toggle'>
            <Nav className="sidebar-nav"
                activeKey="/home"
                onSelect={selectedKey => alert(`selected ${selectedKey}`)}
            >
                <div className="sidebar-sticky"></div>
                <Nav.Item>
                    <Nav.Link href="/home">Active</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="link-1">Link</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="link-2">Link</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="disabled" disabled>
                        Disabled
                    </Nav.Link>
                </Nav.Item>
            </Nav>


        </div>
    );
};
const Sidebar = withRouter(Side);
export default Sidebar