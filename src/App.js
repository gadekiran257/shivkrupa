import React from 'react';
import { Container } from 'postcss';
import { BrowserRouter } from 'react-router-dom';
import './App.scss';
import './scss/styles.scss'

function App() {
  return (
    <div className="masthead">
      <BrowserRouter>
        <div className="App">          
                  <div className="header-space">                    
                    <a className="menu-toggle rounded" href="#"><i className="fas fa-bars"></i></a>
                      <nav id="sidebar-wrapper">
                          <ul className="sidebar-nav">
                              <li className="sidebar-brand"><a href="#page-top">Start Bootstrap</a></li>
                              <li className="sidebar-nav-item"><a href="/index.html">Home</a></li>
                              <li className="sidebar-nav-item"><a href="#login">Login</a></li>
                              <li className="sidebar-nav-item"><a href="#services">Services</a></li>
                              <li className="sidebar-nav-item"><a href="#portfolio">Portfolio</a></li>
                              <li className="sidebar-nav-item"><a href="#contact">Contact</a></li>
                          </ul>
                      </nav>            
                    </div>
            <Container fluid>
                <Form.Row>
                    <Form.Col xs={2} id="sidebar-wrapper">      
                      <Sidebar />
                    </Form.Col>
                    <Form.Col  xs={10} id="page-content-wrapper">
                        this is a test
                    </Form.Col> 
                </Form.Row>
            </Container>
                <footer className="footer text-center">
                    <div className="container px-4 px-lg-5">
                        <ul className="list-inline mb-5">
                            <li className="list-inline-item">
                                <a className="social-link rounded-circle text-white mr-3" href="#!"><i className="icon-social-facebook"></i></a>
                            </li>
                            <li className="list-inline-item">
                                <a className="social-link rounded-circle text-white mr-3" href="#!"><i className="icon-social-twitter"></i></a>
                            </li>
                            <li className="list-inline-item">
                                <a className="social-link rounded-circle text-white" href="#!"><i className="icon-social-github"></i></a>
                            </li>
                        </ul>
                        <p className="text-muted small mb-0">Copyright &copy; Your Website 2021</p>
                    </div>
                  </footer>              
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
